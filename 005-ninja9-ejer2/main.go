package main

import "fmt"

type persona struct {
	Nombre   string
	Apellido string
	Edad     int
}
type humano interface {
	hablar()
}

func (p *persona) hablar() {
	fmt.Println("Los datos de la persona: ", p)

}
func diAlgo(h humano) {
	h.hablar()

}
func main() {
	x := persona{
		Nombre:   "Nahuel",
		Apellido: "Perez",
		Edad:     29,
	}

	diAlgo(&x)
	//diAlgo(x)
	//x.hablar()

}
