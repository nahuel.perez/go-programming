package main

import (
	"fmt"
	"runtime"
)

func main() {
	fmt.Printf("Sistema operativo: %v\nArquitectura: %v", runtime.GOOS, runtime.GOARCH)

}
