package main

import (
	"fmt"
	"sync"
)

func main() {
	contador := 0
	const gs = 100
	var wg sync.WaitGroup
	wg.Add(gs)
	var mu sync.Mutex

	for i := 0; i < gs; i++ {
		go func() {
			mu.Lock()
			v := contador
			v++
			contador = v
			fmt.Println(contador)
			mu.Unlock()
			wg.Done()

		}()

	}
	wg.Wait()
	fmt.Println("cuenta:", contador)

}
