package main

import "fmt"

func main() {

	par := make(chan int)
	impar := make(chan int)
	salir := make(chan int)

	//enviar
	go enviar(par, impar, salir)

	//recibir (como lo hago dentro de main no hace falta que lo invoque como una gorutina)
	recibir(par, impar, salir)

	fmt.Println("Finalizando.")

}

//enviar
func enviar(p, i, s chan<- int) {
	for j := 0; j < 100; j++ {
		if j%2 == 0 {
			p <- j
		} else {
			i <- j
		}
	}
	//Imprime mal por los close que colocamos a continuacion, si los quito se imprime correctamente.
	//close(p)
	//close(i)
	s <- 0
}

//Recibir
func recibir(p, i, s <-chan int) {
	for {
		select {
		case v := <-p:
			fmt.Println("Desde el canal par:", v)
		case v := <-i:
			fmt.Println("Desde el canal impar:", v)
		case v := <-s:
			fmt.Println("Desde el canal salir:", v)
			return
		}
	}
}
