package main

import "fmt"

func main() {
	//unbuffered channel (canal con bufer)
	//el bufer es al cantidad de elementos que se pueden quedar en el canal
	ca := make(chan int, 1)
	ca <- 42

	fmt.Println(<-ca)
}
