package main

import (
	"fmt"
	"runtime"
	"sync"
)

var wg sync.WaitGroup

func main() {
	fmt.Println("numeros de gorutinas al inicio", runtime.NumGoroutine())
	go nahuel()
	go Diego()
	wg.Add(2)
	fmt.Println("numeros de gorutinas al medio", runtime.NumGoroutine())
	wg.Wait()

	fmt.Println("Ellos trabajan en Soflex S.A")
	fmt.Println("numeros de gorutinas al final", runtime.NumGoroutine())
}

func nahuel() {
	fmt.Println("Capacita en SISEP")
	wg.Done()
}

func Diego() {
	fmt.Println("Capacita en SAEweb")
	wg.Done()
}
