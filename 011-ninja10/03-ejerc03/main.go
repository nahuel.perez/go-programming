package main

import (
	"fmt"
)

func main() {
	c := gen()
	recibir(c)

	fmt.Println("A punto de finalizar.")
}

func gen() <-chan int {
	c := make(chan int)

	go func() {
		for i := 0; i < 100; i++ {
			c <- i
		}
		close(c)
	}()
	return c
}

func recibir(c <-chan int) {
	for v := range c {
		fmt.Println(v)
	}
}

//para que funcione fue necesario crear una gorutina en la func gen()
//de esa manera en la gorutina principal se declara y desde la gorutina--
//--secundaria se carga el canal y se cierra.
//se creo la func recibir()
