package main

import (
	"fmt"
)

//  solucion con un canal con Bufer.
//  func main() {
//	c := make(chan int, 1)

//	c <- 42

//	fmt.Println(<-c)

//  }

//Solucion con Gorutina

func main() {
	c := make(chan int, 1)

	go func() {
		c <- 42
	}()

	fmt.Println(<-c)

}
