package main

import (
	"fmt"
)

//Para los 2 casos del ejercicio se corrige simplemente haciendo que la asignacion del canal sea general y no especifica recibir/enviar.

func main() {
	cs := make(chan int)

	go func() {
		cs <- 42
	}()
	fmt.Println(<-cs)

	fmt.Printf("------\n")
	fmt.Printf("cs\t%T\n", cs)
}
